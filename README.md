# Grafana Kubernetes

Am luat un template de deployment si unul de service si le-am modificat.

```
minikube start
helm install grafana-app .
kubectl get all
kubectl port-forward service/grafana-app 3000:3000
```

Am folosit ```kubectl port-forward service/grafana-app 3000:3000``` pentru a putea accesa aplicatia.
Pentru logare am folosit username-ul si password-ul default pentru Grafana.